package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/language/parser"
	"gitlab.com/aleph-project/link"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	if len(os.Args) < 2 {
		fmt.Println("Usage: eval <expr>...")
		return
	}

	var parseMode = parser.ParseDefault
	var parser = new(parser.Parser)

	var port = "localhost:8754"
	// if len(os.Args) > 1 && os.Args[1] != "-" {
	// 	port = os.Args[1]
	// }

	conn, err := link.Dial(false, "tcp", port)
	if err != nil {
		log.Fatal(err)
	}

	str := strings.Join(os.Args[1:], "\n")
	parser.Init([]byte(str), parseMode, conn.Context())

	exprs, errs := parser.ParseAll()
	for _, err := range errs {
		fmt.Printf("%s [char %d]\n", err.Msg, err.Pos)
	}
	if len(errs) > 0 {
		return
	}

	result, err := conn.Execute(exprs)
	if err != nil {
		log.Fatal(err)
	}

	if result.Head().Name() != "CompoundExpression" {
		fmt.Printf("%v\n", result)
		return
	}

	for _, r := range result.GetElements() {
		if r == form.Null {
			continue
		}
		fmt.Printf("%v\n", r)
	}

	conn.Close()
}
