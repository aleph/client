package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"github.com/chzyer/readline"
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/language/parser"
	"gitlab.com/aleph-project/link"
)

const (
	prompt1 = ">>> "
	prompt2 = "... "
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var parseMode = parser.ParseDefault
	var parser = new(parser.Parser)

	var port = "localhost:8754"
	if len(os.Args) > 1 && os.Args[1] != "-" {
		port = os.Args[1]
	}

	conn, err := link.Dial(false, "tcp", port)
	if err != nil {
		log.Fatal(err)
	}

	rl, err := readline.New(prompt1)
	if err != nil {
		log.Fatal(err)
	}

	var lines = make([]string, 0, 4)

	var readlines = func() error {
		lines = lines[:0]

		l, err := rl.Readline()
		if err != nil {
			if err == readline.ErrInterrupt {
				rl.Clean()
			}
			return err
		}

		lines = append(lines, l)

		rl.SetPrompt(prompt2)
		defer rl.SetPrompt(prompt1)

		for {
			l, err := rl.Readline()
			if err != nil {
				if err == readline.ErrInterrupt {
					rl.Clean()
				}
				return err
			}

			if l == "" {
				rl.SetPrompt(prompt1)
				break
			}
			lines = append(lines, l)
		}

		return nil
	}

	for {
		err = readlines()
		if err == readline.ErrInterrupt {
			continue
		} else if err == io.EOF {
			return
		} else if err != nil {
			log.Fatal(err)
		}

		str := strings.Join(lines, "\n") + "\n"
		parser.Init([]byte(str), parseMode, conn.Context())

		exprs, errs := parser.ParseAll()
		for _, err := range errs {
			fmt.Printf("!!! %s [char %d]\n", err.Msg, err.Pos)
		}
		if len(errs) > 0 {
			continue
		}

		result, err := conn.Execute(exprs)
		if err != nil {
			log.Fatal(err)
		}

		if result.Head().Name() != "CompoundExpression" {
			fmt.Printf("<<< %v\n", result)
			continue
		}

		for _, r := range result.GetElements() {
			if r == form.Null {
				continue
			}
			fmt.Printf("<<< %v\n", r)
		}
	}

	conn.Close()
}
