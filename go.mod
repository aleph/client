module gitlab.com/aleph-project/client

require (
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/chzyer/test v0.0.0-20180213035817-a1ea475d72b1 // indirect
	gitlab.com/aleph-project/form v0.2.10
	gitlab.com/aleph-project/language v0.0.0-20181202035158-7b03a787bac6
	gitlab.com/aleph-project/link v0.0.0-20181207051724-e4342ac24b5c
)

replace (
	gitlab.com/aleph-project/language => ../language
	gitlab.com/aleph-project/link => ../link
)
